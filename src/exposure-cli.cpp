/**
 *  @file main.cpp
 *  @brief Main file for running the application
 */
/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


// C/C++ Includes
#include <iostream>
#include <signal.h>
#include <cstring>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <atomic>
#include <math.h>
#include <float.h>


// Modal AI Includes

// ModalAI Library Includes
#include "modal_pipe.h"
#include "modal_pipe_interfaces.h"


#ifdef __ANDROID__
// this is the directory used by the vins-camera-imu-server for data  pipes
std::string CAMERA_CHANNEL_DIR =  "/data/local/tmp/vins_camera/";

#else

// this is the directory used by the vins-camera-imu-server for data  pipes
std::string CAMERA_CHANNEL_DIR =  "/tmp/vins_camera/";

#endif


// The index to use for the IMU and camera channels
static constexpr int CAMERA_CHANNEL_INDEX  	= 1;

const std::string CLIENT_NAME = "exposure-cli";

/** Main Function
 *
 *  @param[in] argc Argument count
 *  @param[in] argv Arguments
 */
int main(int argc, char **argv)
{
	uint32_t exposure_us;
	int16_t gain;

	if( argc < 3 )
	{
		printf( "usage: exposure-cli <exposure_us> <gain> <camera_pipe_dir>\n" );
		return -1;
	}

	exposure_us = atoi(argv[1]);
	gain = atoi(argv[2]);

	if( argc >= 4 )
	{
		CAMERA_CHANNEL_DIR = std::string( argv[3] );
	}

	int ret = pipe_client_init_channel(
		CAMERA_CHANNEL_INDEX, 
		const_cast<char*>(CAMERA_CHANNEL_DIR.c_str()), 
		const_cast<char*>(CLIENT_NAME.c_str()), 0, 1024 );

	if(ret){
		fprintf(stderr, "ERROR opening primary channel:\n");
		pipe_client_print_error(ret);
		if(ret==PIPE_ERROR_SERVER_NOT_AVAILABLE){
			fprintf(stderr, "make sure to start camera-server first!\n");
		}
		return -1;
	}

	// send a hello message back to server for fun
	printf("sending command exposure: %d gain %d\n", exposure_us, gain);
	set_camera_exposure_gain_packet_t exposure;
	exposure.magic_number = CAMERA_MAGIC_NUMBER;
	exposure.exposure_ns = exposure_us*1000;
	exposure.gain = gain;
	pipe_client_send_control_cmd_bytes(
		CAMERA_CHANNEL_INDEX, 
		(char*)&exposure, 
		sizeof(set_camera_exposure_gain_packet_t) );
	
	return 0;
}
