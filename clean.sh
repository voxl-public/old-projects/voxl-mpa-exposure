#!/bin/bash
################################################################################
# Copyright (c) 2020 ModalAI, Inc. All rights reserved.
################################################################################

sudo rm -rf build/
sudo rm -rf build32/
sudo rm -rf build64/
sudo rm -rf ipk/data/
sudo rm -rf ipk/control.tar.gz
sudo rm -rf ipk/data.tar.gz
sudo rm -rf voxl-streamer*.ipk
sudo rm -rf .bash_history

echo ""
echo "Done cleaning"
echo ""
